---
title: "flat_minimal.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(glue)

message("We will focus on Cinnamon squirrels")

primary_fur_color <- "Cinnamon"
message(glue("We will focus on {primary_fur_color} squirrels"))

```

<!--
 You need to run the 'description' chunk in the '0-dev_history.Rmd' file before continuing your code there.
-->

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# My function


# get_message_fur_color()
    
```{r function-get_message_fur_color}
#' Title
#' Get a message with the fur color of interest.
#' 
#' Description
#' 
#' @param primary_fur_color Character. The primary fur color of interest
#' @importFrom glue glue
#' @return Used for side effect. Outputs a message in the console
#' 
#' @export

 
  get_message_fur_color <- function(primary_fur_color) {
  message(glue("We will focus on {primary_fur_color} squirrels"))
}
     

```
  
```{r example-get_message_fur_color}
get_message_fur_color(primary_fur_color = "Black")
```
  
```{r tests-get_message_fur_color}
test_that("get_message_fur_color works", {
  expect_message(
    object = get_message_fur_color(primary_fur_color = "Cinnamon"), 
    regexp = "We will focus on Cinnamon squirrels"
  )
  
  expect_message(
    object = get_message_fur_color(primary_fur_color = "Blue"), 
    regexp = "Blue squirrels do not exist"
  )
  
})

```
  


```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_study_squirrels.Rmd", vignette_name = "Study the squirrels")
```
