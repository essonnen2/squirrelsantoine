# WARNING - Generated by {fusen} from /dev/flat_check_data.Rmd: do not edit by hand

test_that("check_squirrel_data_integrity works", {
  expect_true(inherits(check_squirrel_data_integrity, "function")) 
  
  nyc_squirrels_sample <- readr::read_csv(system.file("nyc_squirrels_sample.csv",
                                                      package = "squirrelsantoine"))
 
  expect_message(
    object = check_squirrel_data_integrity(df_squirrels = nyc_squirrels_sample),
    regexp = "All primary fur color are ok")
})
